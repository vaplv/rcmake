# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

################################################################################
# Append the directory ... to the environment runtime path and store
# the result in _out. The submited elements can be directories or imported
# targets
################################################################################
function(rcmake_append_runtime_dirs _out)
  foreach(_elmt ${ARGN})

    if(IS_DIRECTORY ${_elmt})
       # Append the directory to the _directories list
      list(APPEND _directories ${_elmt})

    elseif(TARGET ${_elmt})
      # Check that the target is imported
      get_target_property(_is_target_imported ${_elmt} IMPORTED)
      if(NOT _is_target_imported)
        message(WARNING
          "Only imported targets can be appended to the runtime environment")
      else()
        # Retrieve the location on disk of the imported target
        if(CMAKE_HOST_WIN32)
          get_target_property(_path ${_elmt} IMPORTED_IMPLIB)
        else()
          get_target_property(_path ${_elmt} IMPORTED_LOCATION)
        endif()
        # Extract the directory from the imported location and append it to the
        # _directories list
        get_filename_component(_dir ${_path} DIRECTORY)
        list(APPEND _directories ${_dir})
      endif()
    endif()
  endforeach()
  list(REMOVE_DUPLICATES _directories)

  # Append the `_directories' list to the environment runtime path
  if(CMAKE_HOST_UNIX)
    # TODO
  elseif(CMAKE_HOST_WIN32)
    string(REGEX REPLACE "[/]+" "\\\\" _paths "${_directories}")
    string(REGEX REPLACE "[\\]*;" ";" _paths "${_paths}")
    string(REGEX REPLACE ";" "\\\\;" _paths "${_paths}")
    string(REGEX REPLACE "[\\]*;" ";" _path_env "$ENV{PATH}")
    string(REGEX REPLACE ";" "\\\\;" _path_env "${_path_env}")
    set(${_out} "${_path_env}\\\\;${_paths}" PARENT_SCOPE)
  endif()
endfunction()

################################################################################
# Set the runtime environment for `_test'
################################################################################
function(rcmake_set_test_runtime_dirs _test _paths)
  if(MSVC)
    set_property(TEST ${_test} PROPERTY ENVIRONMENT "PATH=${${_paths}}")
  endif()
endfunction()

################################################################################
# Helper function that generates the files containing the location of _target
# with respect to the active configuration
################################################################################
function(rcmake_store_locations__ _target _filename_prefix)
  file(GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_filename_prefix}_dbg
    CONTENT $<TARGET_FILE:${_target}>
    CONDITION $<CONFIG:Debug>)
  file(GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_filename_prefix}_mszr
    CONTENT $<TARGET_FILE:${_target}>
    CONDITION $<CONFIG:MinSizeRel>)
  file(GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_filename_prefix}_rdbg
    CONTENT $<TARGET_FILE:${_target}>
    CONDITION $<CONFIG:RelWithDebInfo>)
  file(GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_filename_prefix}
    CONTENT $<TARGET_FILE:${_target}>
    CONDITION $<CONFIG:Release>)
endfunction()

################################################################################
# Helper function that generates the copy prerequisites scripts for `_target'
# Additionnal search paths can be appended with the ... arguments. These
# arguments can be directories or targets whose location directory is going to
# be used as search paths
################################################################################
function(rcmake_generate_prerequisite_scripts__ _target _location_filename)

  # Check that the copy script exists
  find_file(_copy_prerequisites rcmake_copy_prerequisites.cmake.in PATH_SUFFIXES lib/cmake/RCMake)
  if(NOT _copy_prerequisites)
    message(WARNING "Cannot find the rcmake_copy_prerequisites.cmake.in script")
    return()
  endif()

  rcmake_store_locations__(${_target} ${_location_filename})

  set(RCMAKE_RUNTIME_PREFIX_PATH "")
  set(RCMAKE_RUNTIME_LOCATION "")
  foreach(_arg ${ARGN})
    if(IS_DIRECTORY ${_arg})
      # Append the directory to the search paths
      list(APPEND RCMAKE_RUNTIME_PREFIX_PATH ${_arg})
    elseif(TARGET ${_arg})
      # Define the target location at runtime
      rcmake_store_locations__(${_arg} ${_arg}_${_location_filename})
      # Append the `_arg' location files as location whose directory will be
      # used as search paths for the target prerequisites
      list(APPEND RCMAKE_RUNTIME_LOCATION ${_arg}_${_location_filename}_dbg)
      list(APPEND RCMAKE_RUNTIME_LOCATION ${_arg}_${_location_filename}_mszr)
      list(APPEND RCMAKE_RUNTIME_LOCATION ${_arg}_${_location_filename}_rdbg)
      list(APPEND RCMAKE_RUNTIME_LOCATION ${_arg}_${_location_filename})
    endif()
  endforeach()

  # Configure the copy_prerequisites script
  configure_file(${_copy_prerequisites} ${_target}_copy_prerequisites.cmake @ONLY)

endfunction()

################################################################################
# Copy the runtimes of _target in its directory. One can append additionnal
# search paths with ... arguments. These arguments can be directories or
# targets whose location directory is going to be used as search path.
################################################################################
function(rcmake_copy_runtime_libraries _target)
  if(NOT CMAKE_HOST_WIN32)
    return()
  endif()

  rcmake_generate_prerequisite_scripts__(${_target} ${_target}_copy_location ${ARGN})

  add_custom_command(TARGET ${_target} POST_BUILD COMMAND ${CMAKE_COMMAND}
    $<$<CONFIG:Debug>:-DRCMAKE_TARGET_LOCATION=${_target}_copy_location_dbg>
    $<$<CONFIG:MinSizeRel>:-DRCMAKE_TARGET_LOCATION=${_target}_copy_location_mszr>
    $<$<CONFIG:RelWithDebInfo>:-DRCMAKE_TARGET_LOCATION=${_target}_copy_location_rdbg>
    $<$<CONFIG:Release>:-DRCMAKE_TARGET_LOCATION=${_target}_copy_location>
    -P ${_target}_copy_prerequisites.cmake)
endfunction()

################################################################################
# Install the required runtime libraries of a given target into the
# CMAKE_INSTALL_PREFIX directory. Additionnal search paths can be appended with
# ... arguments. These arguments can be directories or targets whose location
# directory is going to be used as search path.
################################################################################
function(rcmake_install_runtime_libraries _target)
  if(NOT CMAKE_HOST_WIN32)
    return()
  endif()

  rcmake_generate_prerequisite_scripts__(${_target} ${_target}_install_location ${ARGN})

  # Install the location file of `_target'
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${_target}_install_location_dbg
    CONFIGURATIONS Debug DESTINATION .
    RENAME ${_target}_install_location)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${_target}_install_location_mszr
    CONFIGURATIONS MinSizeRel DESTINATION .
    RENAME ${_target}_install_location)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${_target}_install_location_rdbg
    CONFIGURATIONS RelWithDebInfo DESTINATION .
    RENAME ${_target}_install_location)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${_target}_install_location
    CONFIGURATIONS Release DESTINATION .)

  # Install the location files of the additionnal targets use as search paths
  # for the prerequisites of `_target'
  foreach(_arg ${ARGN})
    if(TARGET ${_arg})
      install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/${_arg}_${_target}_install_location_dbg
        CONFIGURATIONS Debug DESTINATION .
        RENAME ${_arg}_${_target}_install_location)
      install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/${_arg}_${_target}_install_location_mszr
        CONFIGURATIONS MinSizeRel DESTINATION .
        RENAME ${_arg}_${_target}_install_location)
      install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/${_arg}_${_target}_install_location_rdbg
        CONFIGURATIONS RelWithDebInfo DESTINATION .
        RENAME ${_arg}_${_target}_install_location)
      install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/${_arg}_${_target}_install_location
        CONFIGURATIONS Release DESTINATION .)
    endif()
  endforeach()

  # Invoke the installed "install runtimes" script
  install(CODE "execute_process(COMMAND \"${CMAKE_COMMAND}\"
      -DCMAKE_INSTALL_PREFIX=\${CMAKE_INSTALL_PREFIX}/bin
      -DRCMAKE_TARGET_LOCATION=${CMAKE_INSTALL_PREFIX}/${_target}_install_location
      -P ${CMAKE_CURRENT_BINARY_DIR}/${_target}_copy_prerequisites.cmake)")

  # Remove the location files
  install(CODE "file(REMOVE \${CMAKE_INSTALL_PREFIX}/${_target}_install_location)")
  foreach(_arg ${ARGN})
    if(TARGET ${_arg})
      install(CODE "file(REMOVE \${CMAKE_INSTALL_PREFIX}/${_arg}_${_target}_install_location)")
    endif()
  endforeach()
endfunction()

