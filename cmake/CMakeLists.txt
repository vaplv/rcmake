# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)
project(RCMake NONE)

set(RCMAKE_MODULE_VERSION_MAJOR 0)
set(RCMAKE_MODULE_VERSION_MINOR 4)
set(RCMAKE_MODULE_VERSION_PATCH 1)
configure_file(config_version.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/RCMakeConfigVersion.cmake @ONLY)

set(RCMAKE_FILES
  RCMakeConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/RCMakeConfigVersion.cmake
  config.cmake.in
  config_version.cmake.in
  rcmake.cmake
  rcmake_cl.cmake
  rcmake_copy_prerequisites.cmake.in
  rcmake_g++.cmake
  rcmake_gcc.cmake
  rcmake_runtime.cmake
  rcmake_tcc.cmake
  toolchain_i686-w64-mingw32.cmake
  toolchain_x86_64-w64-mingw32.cmake)

set(RCMAKE_FILES_DOC
  ../COPYING
  ../README.md)

install(FILES ${RCMAKE_FILES} version.h.in DESTINATION lib/cmake/RCMake)
install(FILES ${RCMAKE_FILES_DOC} DESTINATION share/doc/rcmake)
