# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

find_path(RCMAKE_SOURCE_DIR rcmake.cmake PATH_SUFFIXES lib/cmake/RCMake
  NO_CMAKE_FIND_ROOT_PATH)
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(RCMake DEFAULT_MSG RCMAKE_SOURCE_DIR)
