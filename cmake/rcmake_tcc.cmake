# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

if(NOT ${CMAKE_C_COMPILER_ID} STREQUAL "TinyCC")
  message(FATAL_ERROR "Unsupported compiler")
endif()

################################################################################
# CC compilation flags
################################################################################
set(C_FLAGS "-pedantic -std=c89 -fvisibility=hidden -fstrict-aliasing -fPIC")
set(C_FLAGS_WARN "-Wall -Wextra -Wcast-align -Wmissing-declarations -Wmissing-prototypes -Wconversion -Wno-long-long")

################################################################################
# Set the CMake cached variables
################################################################################
get_property(_doc CACHE CMAKE_C_FLAGS PROPERTY HELPSTRING)
set(CMAKE_C_FLAGS "${C_FLAGS} ${C_FLAGS_WARN}" CACHE STRING "${_doc}" FORCE)

if(NOT CMAKE_BUILD_TYPE)
  get_property(_doc CACHE CMAKE_BUILD_TYPE PROPERTY HELPSTRING)
  set(CMAKE_BUILD_TYPE "DEBUG" CACHE STRING ${_doc} FORCE)
endif()

