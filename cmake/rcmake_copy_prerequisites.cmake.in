# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Script arguments:
#   RCMAKE_TARGET_LOCATION: name of the file that stores the target location


cmake_minimum_required(VERSION 3.1)
include(GetPrerequisites)

# Check the RCMAKE_TARGET_LOCATION argument
if(NOT RCMAKE_TARGET_LOCATION)
  message(FATAL_ERROR "RCMAKE_TARGET_LOCATION is not defined")
endif()

# Define the target location and the directory in which its prerequisites are
# going to be copied
file(READ ${RCMAKE_TARGET_LOCATION} _location)
if(CMAKE_INSTALL_PREFIX)
  set(_target_dir ${CMAKE_INSTALL_PREFIX})
else()
  get_filename_component(_target_dir ${_location} PATH)
endif()

# Define in which directories the prerequisites are searched
list(APPEND _prefix_path
  "${_target_dir}"
  "@CMAKE_PREFIX_PATH@"
  "@CMAKE_INSTALL_PREFIX@"
  "@RCMAKE_RUNTIME_PREFIX_PATH@") # User submitted additional paths

foreach(_runtime_location @RCMAKE_RUNTIME_LOCATION@)
  if(EXISTS ${_runtime_location})
    file(READ ${_runtime_location} _tmp)
    get_filename_component(_tmp ${_tmp} PATH)
    list(APPEND _prefix_path ${_tmp})
  endif()
endforeach()

list(REMOVE_DUPLICATES _prefix_path)
foreach(_path ${_prefix_path})
  list(APPEND _search_dirs ${_path})
  list(APPEND _suffix_path lib Lib LIB bin Bin BIN)
  foreach(_suffix ${_suffix_path})
    list(APPEND _search_dirs ${_path}/${_suffix})
  endforeach()
endforeach()

# Define the Prerequisites of the target RCMAKE_TARGET
get_prerequisites(${_location}
  _prerequisites 1 1 "${_target_dir}" "${_search_dirs}")

# Copy the prerequisites
foreach(_runtime ${_prerequisites})
  get_filename_component(_name ${_runtime} NAME)
  if(IS_ABSOLUTE ${_runtime})
    set(_runtime_${_name} ${_runtime})
  else()
    find_file(_runtime_${_name} ${_name} PATHS ${_search_dirs} ${_target_dir} NO_DEFAULT_PATH)
  endif()
  message(STATUS "Copy runtime: ${_target_dir}/${_name}")
  if(NOT _runtime_${_name})
    message(SEND_ERROR "Can't find the ${_name} runtime")
  else()
    execute_process(COMMAND ${CMAKE_COMMAND} -E copy_if_different
      ${_runtime_${_name}} ${_target_dir})
  endif()
endforeach()

