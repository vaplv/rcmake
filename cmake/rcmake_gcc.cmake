# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

if(NOT CMAKE_COMPILER_IS_GNUCC)
  message(FATAL_ERROR "Unsupported compiler")
endif()

# Register the BUILD_32BIT cached variable
if(NOT MINGW AND CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(BUILD_32BIT ${BUILD_32BIT} CACHE BOOL "Force generation of 32-bits binaries")
  if(NOT DEFINED BUILD_32BIT__)
    set(BUILD_32BIT__ ${BUILD_32BIT} CACHE INTERNAL "")
  endif()
  unset(BUILD_32BIT)
endif()

################################################################################
# CC compilation flags
################################################################################
set(C_FLAGS "-pedantic -std=c89 -fvisibility=hidden -fstrict-aliasing")
set(C_FLAGS_WARN "-Wall -Wextra -Wcast-align -Wmissing-declarations -Wmissing-prototypes -Wconversion -Wno-long-long -Wshadow")

if(NOT MINGW)
  set(C_FLAGS "${C_FLAGS} -fPIC")
  if(BUILD_32BIT)
    set(C_FLAGS "${C_FLAGS} -m32")
    set(C_FLAGS_LINK "-m32")
  endif(BUILD_32BIT)

  if("${BUILD_32BIT}" STREQUAL "${BUILD_32BIT__}")
    unset(_force_flags_update)
  else()
    set(_force_flags_update 1)
    set(BUILD_32BIT__ ${BUILD_32BIT} CACHE INTERNAL  "")
  endif()
endif()

if(NOT BUILD_32BIT OR NOT CMAKE_SIZEOF_VOID_P EQUAL 4)
  set(C_FLAGS_WARN "${C_FLAGS_WARN} -Wl,--no-undefined")
endif()

################################################################################
# Set the CMAKE_<CXX_FLAGS|SHARED_LINKER_FLAGS|BUILD_TYPE> cached variables
################################################################################
string(STRIP "${CMAKE_C_FLAGS}" _str)
if(NOT _str OR _force_flags_update)
  get_property(_doc CACHE CMAKE_C_FLAGS PROPERTY HELPSTRING)
  set(CMAKE_C_FLAGS "${C_FLAGS} ${C_FLAGS_WARN}" CACHE STRING "${_doc}" FORCE)
endif()

string(STRIP "${CMAKE_SHARED_LINKER_FLAGS}" _str)
if(NOT _str OR _force_flags_update)
  get_property(_doc CACHE CMAKE_SHARED_LINKER_FLAGS PROPERTY HELPSTRING)
  set(CMAKE_SHARED_LINKER_FLAGS "${C_FLAGS_LINK}" CACHE STRING "${_doc}" FORCE)
endif()

if(NOT CMAKE_BUILD_TYPE)
  get_property(_doc CACHE CMAKE_BUILD_TYPE PROPERTY HELPSTRING)
  set(CMAKE_BUILD_TYPE "DEBUG" CACHE STRING ${_doc} FORCE)
endif()

