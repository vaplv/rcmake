# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

if(NOT MSVC)
  message(FATAL_ERROR "Unsupported compiler")
endif()

if(${MSVC_VERSION} VERSION_LESS "1700")
  message(WARNING "The first supported Visual Studio is version VS 11.0")
endif()

function(set_cmake _flag _value)
  get_property(_doc CACHE ${_flag} PROPERTY HELPSTRING)
  set(${_flag} ${_value} CACHE STRING "${_doc}" FORCE)
endfunction()

set(COMMON_FLAGS "/W4 /Gm- /EHsc /Zi /Oi /DNOMINMAX /D_CRT_SECURE_NO_WARNINGS /D_SCL_SECURE_NO_WARNINGS")

# C compilation options
set_cmake(CMAKE_C_FLAGS "/TC ${COMMON_FLAGS}")
set_cmake(CMAKE_C_FLAGS_DEBUG "/DEBUG /RTC1 /MP /Od /Ob0 /DDEBUG /fp:strict /MDd")
set_cmake(CMAKE_C_FLAGS_RELEASE "/MP /DNDEBUG /fp:strict /MD /O2 /GL /GS-")

# CXX compilation options
set_cmake(CMAKE_CXX_FLAGS "/TP ${COMMON_FLAGS}")
set_cmake(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
set_cmake(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")

# Linker options
set_cmake(CMAKE_EXE_LINKER_FLAGS_RELEASE "/LTCG /INCREMENTAL:NO")
set_cmake(CMAKE_SHARED_LINKER_FLAGS_RELEASE "/LTCG /INCREMENTAL:NO")

