# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

set(CMAKE_DEBUG_POSTFIX "-dbg")
set(CMAKE_RELWITHDEBINFO_POSTFIX "-rdbg")
set(CMAKE_MINSIZEREL_POSTFIX "-mszr")
get_filename_component(RCMAKE_SOURCE_DIR ${CMAKE_CURRENT_LIST_FILE} PATH)

if(CMAKE_COMPILER_IS_GNUCC)
  include(rcmake_gcc)
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
  include(rcmake_g++)
endif()

if(MSVC)
  include(rcmake_cl)
endif()

if(${CMAKE_C_COMPILER_ID} STREQUAL "TinyCC")
  include(rcmake_tcc)
endif()

################################################################################
# Helper macros
################################################################################
# Prepend each file in the `_files' list by `_path'
function(rcmake_prepend_path _files _path)
  foreach(_file ${${_files}})
    list(APPEND _tmp ${_path}/${_file})
  endforeach(_file)
  set(${_files} ${_tmp} PARENT_SCOPE)
endfunction(rcmake_prepend_path)

# Setup the `_target' development module. Generate the CMake module
# ${_name}Config[Version].cmake files as well as the public library version
# header whose name is extracted from _header_version. The generated CMake
# files will be automatically installed in the `lib/cmake' directory while the
# version header will be deployed in the path defined in _header_version
# prefixed by `include'
function(rcmake_setup_devel _target _name _version _header_version)
  find_file(_config_template config.cmake.in PATHS ${RCMAKE_SOURCE_DIR}
    NO_CMAKE_FIND_ROOT_PATH)
  find_file(_config_version_template config_version.cmake.in PATHS ${RCMAKE_SOURCE_DIR}
    NO_CMAKE_FIND_ROOT_PATH)
  find_file(_header_version_template version.h.in PATHS ${RCMAKE_SOURCE_DIR}
    NO_CMAKE_FIND_ROOT_PATH)

  get_filename_component(_header_file ${_header_version} NAME)
  get_filename_component(_header_dir ${_header_version} PATH)
  if(IS_ABSOLUTE ${_header_dir})
    message(FATAL_ERROR "Expecting a relative path ${_header_dir}")
  endif()

  # Configure the cmake module file
  get_target_property(_target_type ${_target} TYPE)
  if(NOT "${_target_type}" STREQUAL "SHARED_LIBRARY")
    message(FATAL_ERROR "${_target} is not shared library")
  endif()
  set(RCMAKE_MODULE_PREFIX ${_name})
  set(RCMAKE_MODULE_HEADER ${_header_version})
  set(RCMAKE_MODULE_LIBRARY ${_target})

  configure_file(${_config_template}
    ${CMAKE_CURRENT_BINARY_DIR}/${_name}Config.cmake @ONLY)

  # Configure the version header
  string(REGEX MATCH "^[0-9]+\\.[0-9]+\\.[0-9]+$" _version ${_version})
  if(NOT _version)
    message(FATAL_ERROR "Bad ${_lib} version number. Expecting [0-9]+.[0-9]+.[0.9]+")
  endif()
  string(REGEX REPLACE "^([0-9]+)\\.[0-9]+\\.[0-9]+$" "\\1" _major ${_version})
  string(REGEX REPLACE "^[0-9]+\\.([0-9]+)\\.[0-9]+$" "\\1" _minor ${_version})
  string(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+)$" "\\1" _patch ${_version})
  set(RCMAKE_MODULE_VERSION_MAJOR ${_major})
  set(RCMAKE_MODULE_VERSION_MINOR ${_minor})
  set(RCMAKE_MODULE_VERSION_PATCH ${_patch})
  configure_file(${_config_version_template}
    ${CMAKE_CURRENT_BINARY_DIR}/${_name}ConfigVersion.cmake @ONLY)
  configure_file(${_header_version_template}
    ${CMAKE_CURRENT_BINARY_DIR}/${_header_file} @ONLY)

  # Define the install directive for the generated files
  install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/${_header_file}
    DESTINATION include/${_header_dir})
  install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/${_name}Config.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/${_name}ConfigVersion.cmake
    DESTINATION lib/cmake/${_name})
endfunction(rcmake_setup_devel)

