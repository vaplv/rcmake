# Copyright (C) 2013-2017, 2021 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)

if(NOT CMAKE_COMPILER_IS_GNUCXX)
  message(FATAL_ERROR "Unsupported compiler")
endif()

################################################################################
# CXX compilation flags
################################################################################
set(CXX_FLAGS "-pedantic -std=c++98 -fno-rtti -fvisibility=hidden -fstrict-aliasing -fPIC")
set(CXX_FLAGS_WARN "-Wall -Wextra -Wcast-align -Wconversion -Wshadow -Wl,--no-undefined")
set(CXX_FLAGS_LINK "")

################################################################################
# Set the CMAKE_<CXX_FLAGS|SHARED_LINKER_FLAGS|BUILD_TYPE> cached variables
################################################################################
string(STRIP "${CMAKE_CXX_FLAGS}" _str)
if(NOT _str)
  get_property(_doc CACHE CMAKE_CXX_FLAGS PROPERTY HELPSTRING)
  set(CMAKE_CXX_FLAGS "${CXX_FLAGS} ${CXX_FLAGS_WARN}" CACHE STRING "${_doc}" FORCE)
endif()

string(STRIP "${CMAKE_SHARED_LINKER_FLAGS}" _str)
if(NOT _str)
  get_property(_doc CACHE CMAKE_SHARED_LINKER_FLAGS PROPERTY HELPSTRING)
  set(CMAKE_SHARED_LINKER_FLAGS "${CXX_FLAGS_LINK}" CACHE STRING "${_doc}" FORCE)
endif()

if(NOT CMAKE_BUILD_TYPE)
  get_property(_doc CACHE CMAKE_BUILD_TYPE PROPERTY HELPSTRING)
  set(CMAKE_BUILD_TYPE "DEBUG" CACHE STRING ${_doc} FORCE)
endif()

